package com.odilo.droid.console.params;

import java.util.HashMap;

public class ConsoleParams extends HashMap<String, ConsoleParam> {

    public ConsoleParams(String[] args) {
        loadArgs(args);
    }

    public void loadArgs(String[] args) {
        for (String arg: args) {
            loadParam(arg);
        }
    }

    public void loadParam(String arg) {

        try {
            ConsoleParam param = new ConsoleParam(arg);
            put(param.getType(), param);
        } catch (Exception e) {
        }
    }
}
