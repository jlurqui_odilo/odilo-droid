package com.odilo.droid.console.params;

import jdk.nashorn.internal.runtime.regexp.joni.exception.SyntaxException;

import java.util.Set;

public class ConsoleParam {

    public static final String PARAM_INIT = "-";
    private static final String VALID_TYPES  = "SsTt";
    private static final String VALID_TYPES_PATTERN  = PARAM_INIT + "^[" + VALID_TYPES + "]\\S*$";

    private String type;
    private String value;

    public ConsoleParam(String arg) throws Exception {

        arg = arg.trim();

        if (arg.matches(VALID_TYPES_PATTERN)) {

            setType(arg.substring(1, 1).toLowerCase());
            setValue(arg.substring(2));
        } else {
            throw new SyntaxException(String.format("Param %s no cumple con la sintaxis", arg));
        }
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
