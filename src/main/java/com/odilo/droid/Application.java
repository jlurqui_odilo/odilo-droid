package com.odilo.droid;

import com.odilo.droid.console.params.ConsoleParam;
import com.odilo.droid.console.params.ConsoleParams;
import com.odilo.droid.xml.XmlProcessor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;

/**
 * Created by Javier Lurquí on 04/08/2020.
 */
public class Application {

    public static final String DROID_SIGNATURE_FILE_V96 = "droid/DROID_SignatureFile_V96.xml";
    public static final String DROID_SIGNATURE_TARGET = "target.js";

    private static final Logger log = LoggerFactory.getLogger(Application.class);

    private static ConsoleParams params;

    public static void main(String[] args) {

        try {

            log.info("Iniciando procesamiento");

            params = new ConsoleParams(args);

            XmlProcessor.process(getXmlFileName(params), getTargetFileName(params));

            log.info("Proceso finalizado correctamente");

        } catch (Exception e) {
            log.error("Proceso finalizado con errores", e);
        }
    }

    private static String getXmlFileName(ConsoleParams params) {

        ConsoleParam source = params.get("s");

        if (source != null && (new File(source.getValue())).exists()) {
            return source.getValue();
        }

        return Application.class.getClassLoader().getResource(DROID_SIGNATURE_FILE_V96).getPath();
    }

    private static String getTargetFileName(ConsoleParams params) {

        String result;
        String droidPath = Application.class.getClassLoader().getResource(DROID_SIGNATURE_FILE_V96).getPath();
        ConsoleParam target = params.get("t");
        File droidFile = new File(droidPath);
        droidPath = droidFile.getParentFile().getPath();

        if (!droidPath.endsWith(File.separator)) {
            droidPath = droidPath.concat(File.separator);
        }

        if (target != null) {
            result = target.getValue();

            if (!result.contains(File.separator)) {
                result = droidPath.concat(result);
            }
        } else {
            result = droidPath.concat(DROID_SIGNATURE_TARGET);
        }

        return result;
    }
}
