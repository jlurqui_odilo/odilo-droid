package com.odilo.droid.xml;

import com.odilo.droid.xsd.FFSignatureFileType;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.JAXBException;
import javax.xml.bind.Unmarshaller;

/**
 * Created by Javier Lurquí on 04/08/2020.
 */
public class XmlReader {

    protected static JAXBContext jaxbContext;

    protected Unmarshaller unmarshaller;

    public static Unmarshaller getNewUnmarshaller() throws JAXBException {
        return (new XmlReader()).getUnmarshaller();
    }

    public XmlReader() throws JAXBException {
        unmarshaller = getJAXBContext().createUnmarshaller();
    }

    public Unmarshaller getUnmarshaller() {
        return unmarshaller;
    }

    public JAXBContext getJAXBContext() throws JAXBException {

        if (jaxbContext == null) {
            jaxbContext = JAXBContext.newInstance(FFSignatureFileType.class);
        }

        return jaxbContext;
    }
}
