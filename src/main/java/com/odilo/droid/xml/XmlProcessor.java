package com.odilo.droid.xml;

import com.odilo.droid.xsd.FFSignatureFileType;
import com.odilo.droid.xsd.FileFormatType;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.JAXBException;
import javax.xml.namespace.QName;
import java.io.*;
import java.util.List;

/**
 * Created by Javier Lurquí on 04/08/2020.
 */
public class XmlProcessor {

    private Logger log = LoggerFactory.getLogger(this.getClass());

    public static String MONGODB_FORMATTYPE_INSERT_START = "db.format.insert([";
    public static String MONGODB_FORMATTYPE_INSERT_END = "]);\r";
    public static String MONGODB_FORMATTYPE_INSERT = "{\"_class\":\"com.odilo.preserver.domain.Format\"," +
            "\"extension\":\"%s\"," +
            "\"version\":\"%s\"," +
            "\"comunName\":\"%s\"," +
            "\"formalName\":\"%s\"," +
            "\"type\":\"%s\"," +
            "\"state\":\"%s\"," +
            "\"formatType\":[]," +
            "\"additionalInformation\":\"Droid\"," +
            "\"source\":\"droid\"," +
            "\"puid\":\"%s\"," +
            "\"created\":new Date()," +
            "\"lastModified\":new Date()},\r";


    private final static QName FileFormatTypeExtension_QNAME = new QName("http://www.nationalarchives.gov.uk/pronom/SignatureFile", "Extension");

    public static FFSignatureFileType process(String xmlFileName, String targetFileName) throws Exception {

        XmlProcessor processor = new XmlProcessor();
        FFSignatureFileType result = processor.getFFSignatureFileType(xmlFileName);

        generateFile(result.getFileFormatCollection().getFileFormat(), targetFileName, processor.getLog());

        processor.getLog().info(String.format("Procesados %s formatos de archivo en %s",
                result.getFileFormatCollection().getFileFormat().size(), targetFileName));

        return result;
    }

    public FFSignatureFileType getFFSignatureFileType(String xmlFileName) throws Exception {

        checkExecution(xmlFileName);

        try {
            return ((JAXBElement<FFSignatureFileType>) XmlReader.getNewUnmarshaller().unmarshal(new File(xmlFileName)))
                .getValue();
        } catch (JAXBException e) {
            log.error("Error al obtener el objeto FFSignatureFileType", e);
            throw e;
        }
    }

    public Logger getLog() {
        return log;
    }

    private void checkExecution(String fileName) throws Exception {

        if (fileName == null || fileName.isEmpty()) {
            throw new Exception("Debe indicar un archivo de firmas válido");
        }
    }

    public static void generateFile(List<FileFormatType> formatTypes, String targetFileName, Logger log) throws IOException {

        File targetFile = new File(targetFileName);

        if (targetFile.exists()) {
            targetFile.delete();
        }

        FileOutputStream tfs = new FileOutputStream(targetFile);
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(tfs));
        int i = 0;

        bw.write(MONGODB_FORMATTYPE_INSERT_START);

        for (FileFormatType formatType: formatTypes) {

            for (Serializable content: formatType.getContent()) {

                if (content.getClass().equals(JAXBElement.class)) {
                    JAXBElement<String> jbe = (JAXBElement<String>) content;

                    if (jbe.getName().equals(FileFormatTypeExtension_QNAME)) {
                        bw.write(generateLine(jbe.getValue(), formatType));
                    }
                }
            }
        }

        bw.write(MONGODB_FORMATTYPE_INSERT_END);
        bw.close();
    }

//    {
//            "_class" : "com.odilo.preserver.domain.Format",
//            "extension" : "%s",
//            "version" : "%s",
//            "comunName" : "%s",
//            "formalName" : "%s",
//            "type" : "%s",
//            "state" : "%s",
//            "formatType" : [],
//            "additionalInformation" : "Droid",
//            "source" : "droid",
//            "puid": "%s",
//            "created" : new Date(),
//            "lastModified" : new Date()
//    }
    public static String generateLine(String extension, FileFormatType formatType) {
        return String.format(MONGODB_FORMATTYPE_INSERT, extension,
                formatType.getVersion(),
                formatType.getName(),
                formatType.getName(),
                getValueOrNull(formatType.getMIMEType(), ""),
                "NOT_SUPPORTED",
                formatType.getPUID()
        );
    }

    public static String getValueOrNull(String value, String nullValue) {
        return (value == null) ? nullValue : value;
    }
}