//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.04 a las 11:48:47 AM CEST 
//


package com.odilo.droid.xsd;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the com.odilo.droid.xsd package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _FFSignatureFile_QNAME = new QName("http://www.nationalarchives.gov.uk/pronom/SignatureFile", "FFSignatureFile");
    private final static QName _FileFormatTypeInternalSignatureID_QNAME = new QName("http://www.nationalarchives.gov.uk/pronom/SignatureFile", "InternalSignatureID");
    private final static QName _FileFormatTypeExtension_QNAME = new QName("http://www.nationalarchives.gov.uk/pronom/SignatureFile", "Extension");
    private final static QName _FileFormatTypeHasPriorityOverFileFormatID_QNAME = new QName("http://www.nationalarchives.gov.uk/pronom/SignatureFile", "HasPriorityOverFileFormatID");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: com.odilo.droid.xsd
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link FFSignatureFileType }
     * 
     */
    public FFSignatureFileType createFFSignatureFileType() {
        return new FFSignatureFileType();
    }

    /**
     * Create an instance of {@link InternalSignatureCollectionType }
     * 
     */
    public InternalSignatureCollectionType createInternalSignatureCollectionType() {
        return new InternalSignatureCollectionType();
    }

    /**
     * Create an instance of {@link SubSequenceType }
     * 
     */
    public SubSequenceType createSubSequenceType() {
        return new SubSequenceType();
    }

    /**
     * Create an instance of {@link ByteSequenceType }
     * 
     */
    public ByteSequenceType createByteSequenceType() {
        return new ByteSequenceType();
    }

    /**
     * Create an instance of {@link FileFormatType }
     * 
     */
    public FileFormatType createFileFormatType() {
        return new FileFormatType();
    }

    /**
     * Create an instance of {@link RightFragmentType }
     * 
     */
    public RightFragmentType createRightFragmentType() {
        return new RightFragmentType();
    }

    /**
     * Create an instance of {@link ShiftType }
     * 
     */
    public ShiftType createShiftType() {
        return new ShiftType();
    }

    /**
     * Create an instance of {@link InternalSignatureType }
     * 
     */
    public InternalSignatureType createInternalSignatureType() {
        return new InternalSignatureType();
    }

    /**
     * Create an instance of {@link FileFormatCollectionType }
     * 
     */
    public FileFormatCollectionType createFileFormatCollectionType() {
        return new FileFormatCollectionType();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FFSignatureFileType }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.nationalarchives.gov.uk/pronom/SignatureFile", name = "FFSignatureFile")
    public JAXBElement<FFSignatureFileType> createFFSignatureFile(FFSignatureFileType value) {
        return new JAXBElement<FFSignatureFileType>(_FFSignatureFile_QNAME, FFSignatureFileType.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.nationalarchives.gov.uk/pronom/SignatureFile", name = "InternalSignatureID", scope = FileFormatType.class)
    public JAXBElement<String> createFileFormatTypeInternalSignatureID(String value) {
        return new JAXBElement<String>(_FileFormatTypeInternalSignatureID_QNAME, String.class, FileFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.nationalarchives.gov.uk/pronom/SignatureFile", name = "Extension", scope = FileFormatType.class)
    public JAXBElement<String> createFileFormatTypeExtension(String value) {
        return new JAXBElement<String>(_FileFormatTypeExtension_QNAME, String.class, FileFormatType.class, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link String }{@code >}}
     * 
     */
    @XmlElementDecl(namespace = "http://www.nationalarchives.gov.uk/pronom/SignatureFile", name = "HasPriorityOverFileFormatID", scope = FileFormatType.class)
    public JAXBElement<String> createFileFormatTypeHasPriorityOverFileFormatID(String value) {
        return new JAXBElement<String>(_FileFormatTypeHasPriorityOverFileFormatID_QNAME, String.class, FileFormatType.class, value);
    }

}
