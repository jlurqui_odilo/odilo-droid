//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.04 a las 11:48:47 AM CEST 
//


package com.odilo.droid.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para FFSignatureFileType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="FFSignatureFileType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="InternalSignatureCollection" type="{http://www.nationalarchives.gov.uk/pronom/SignatureFile}InternalSignatureCollectionType"/>
 *         &lt;element name="FileFormatCollection" type="{http://www.nationalarchives.gov.uk/pronom/SignatureFile}FileFormatCollectionType"/>
 *       &lt;/sequence>
 *       &lt;attribute name="DateCreated" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Version" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "FFSignatureFileType", propOrder = {
    "internalSignatureCollection",
    "fileFormatCollection"
})
public class FFSignatureFileType {

    @XmlElement(name = "InternalSignatureCollection", required = true)
    protected InternalSignatureCollectionType internalSignatureCollection;
    @XmlElement(name = "FileFormatCollection", required = true)
    protected FileFormatCollectionType fileFormatCollection;
    @XmlAttribute(name = "DateCreated")
    protected String dateCreated;
    @XmlAttribute(name = "Version")
    protected String version;

    /**
     * Obtiene el valor de la propiedad internalSignatureCollection.
     * 
     * @return
     *     possible object is
     *     {@link InternalSignatureCollectionType }
     *     
     */
    public InternalSignatureCollectionType getInternalSignatureCollection() {
        return internalSignatureCollection;
    }

    /**
     * Define el valor de la propiedad internalSignatureCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link InternalSignatureCollectionType }
     *     
     */
    public void setInternalSignatureCollection(InternalSignatureCollectionType value) {
        this.internalSignatureCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad fileFormatCollection.
     * 
     * @return
     *     possible object is
     *     {@link FileFormatCollectionType }
     *     
     */
    public FileFormatCollectionType getFileFormatCollection() {
        return fileFormatCollection;
    }

    /**
     * Define el valor de la propiedad fileFormatCollection.
     * 
     * @param value
     *     allowed object is
     *     {@link FileFormatCollectionType }
     *     
     */
    public void setFileFormatCollection(FileFormatCollectionType value) {
        this.fileFormatCollection = value;
    }

    /**
     * Obtiene el valor de la propiedad dateCreated.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDateCreated() {
        return dateCreated;
    }

    /**
     * Define el valor de la propiedad dateCreated.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDateCreated(String value) {
        this.dateCreated = value;
    }

    /**
     * Obtiene el valor de la propiedad version.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getVersion() {
        return version;
    }

    /**
     * Define el valor de la propiedad version.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setVersion(String value) {
        this.version = value;
    }

}
