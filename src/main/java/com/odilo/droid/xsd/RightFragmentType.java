//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.04 a las 11:48:47 AM CEST 
//


package com.odilo.droid.xsd;

import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlType;
import javax.xml.bind.annotation.XmlValue;


/**
 * <p>Clase Java para RightFragmentType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="RightFragmentType">
 *   &lt;simpleContent>
 *     &lt;extension base="&lt;http://www.w3.org/2001/XMLSchema>string">
 *       &lt;attribute name="MaxOffset" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="MinOffset" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Position" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/extension>
 *   &lt;/simpleContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "RightFragmentType", propOrder = {
    "value"
})
public class RightFragmentType {

    @XmlValue
    protected String value;
    @XmlAttribute(name = "MaxOffset")
    protected String maxOffset;
    @XmlAttribute(name = "MinOffset")
    protected String minOffset;
    @XmlAttribute(name = "Position")
    protected String position;

    /**
     * Obtiene el valor de la propiedad value.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getValue() {
        return value;
    }

    /**
     * Define el valor de la propiedad value.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setValue(String value) {
        this.value = value;
    }

    /**
     * Obtiene el valor de la propiedad maxOffset.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMaxOffset() {
        return maxOffset;
    }

    /**
     * Define el valor de la propiedad maxOffset.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMaxOffset(String value) {
        this.maxOffset = value;
    }

    /**
     * Obtiene el valor de la propiedad minOffset.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinOffset() {
        return minOffset;
    }

    /**
     * Define el valor de la propiedad minOffset.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinOffset(String value) {
        this.minOffset = value;
    }

    /**
     * Obtiene el valor de la propiedad position.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Define el valor de la propiedad position.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

}
