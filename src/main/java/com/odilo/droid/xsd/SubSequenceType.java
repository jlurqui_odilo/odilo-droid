//
// Este archivo ha sido generado por la arquitectura JavaTM para la implantación de la referencia de enlace (JAXB) XML v2.2.8-b130911.1802 
// Visite <a href="http://java.sun.com/xml/jaxb">http://java.sun.com/xml/jaxb</a> 
// Todas las modificaciones realizadas en este archivo se perderán si se vuelve a compilar el esquema de origen. 
// Generado el: 2020.08.04 a las 11:48:47 AM CEST 
//


package com.odilo.droid.xsd;

import java.util.ArrayList;
import java.util.List;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlAttribute;
import javax.xml.bind.annotation.XmlElement;
import javax.xml.bind.annotation.XmlType;


/**
 * <p>Clase Java para SubSequenceType complex type.
 * 
 * <p>El siguiente fragmento de esquema especifica el contenido que se espera que haya en esta clase.
 * 
 * <pre>
 * &lt;complexType name="SubSequenceType">
 *   &lt;complexContent>
 *     &lt;restriction base="{http://www.w3.org/2001/XMLSchema}anyType">
 *       &lt;sequence>
 *         &lt;element name="Sequence" type="{http://www.w3.org/2001/XMLSchema}string"/>
 *         &lt;element name="DefaultShift">
 *           &lt;simpleType>
 *             &lt;restriction base="{http://www.w3.org/2001/XMLSchema}string">
 *               &lt;enumeration value="5"/>
 *               &lt;enumeration value="7"/>
 *               &lt;enumeration value="-2"/>
 *               &lt;enumeration value="9"/>
 *               &lt;enumeration value="-6"/>
 *               &lt;enumeration value="20"/>
 *               &lt;enumeration value="25"/>
 *               &lt;enumeration value="26"/>
 *               &lt;enumeration value="8"/>
 *             &lt;/restriction>
 *           &lt;/simpleType>
 *         &lt;/element>
 *         &lt;element name="Shift" type="{http://www.nationalarchives.gov.uk/pronom/SignatureFile}ShiftType" maxOccurs="unbounded" minOccurs="0"/>
 *         &lt;element name="RightFragment" type="{http://www.nationalarchives.gov.uk/pronom/SignatureFile}RightFragmentType" minOccurs="0"/>
 *       &lt;/sequence>
 *       &lt;attribute name="MinFragLength" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="Position" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SubSeqMaxOffset" type="{http://www.w3.org/2001/XMLSchema}string" />
 *       &lt;attribute name="SubSeqMinOffset" type="{http://www.w3.org/2001/XMLSchema}string" />
 *     &lt;/restriction>
 *   &lt;/complexContent>
 * &lt;/complexType>
 * </pre>
 * 
 * 
 */
@XmlAccessorType(XmlAccessType.FIELD)
@XmlType(name = "SubSequenceType", propOrder = {
    "sequence",
    "defaultShift",
    "shift",
    "rightFragment"
})
public class SubSequenceType {

    @XmlElement(name = "Sequence", required = true)
    protected String sequence;
    @XmlElement(name = "DefaultShift", required = true)
    protected String defaultShift;
    @XmlElement(name = "Shift")
    protected List<ShiftType> shift;
    @XmlElement(name = "RightFragment")
    protected RightFragmentType rightFragment;
    @XmlAttribute(name = "MinFragLength")
    protected String minFragLength;
    @XmlAttribute(name = "Position")
    protected String position;
    @XmlAttribute(name = "SubSeqMaxOffset")
    protected String subSeqMaxOffset;
    @XmlAttribute(name = "SubSeqMinOffset")
    protected String subSeqMinOffset;

    /**
     * Obtiene el valor de la propiedad sequence.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSequence() {
        return sequence;
    }

    /**
     * Define el valor de la propiedad sequence.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSequence(String value) {
        this.sequence = value;
    }

    /**
     * Obtiene el valor de la propiedad defaultShift.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getDefaultShift() {
        return defaultShift;
    }

    /**
     * Define el valor de la propiedad defaultShift.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setDefaultShift(String value) {
        this.defaultShift = value;
    }

    /**
     * Gets the value of the shift property.
     * 
     * <p>
     * This accessor method returns a reference to the live list,
     * not a snapshot. Therefore any modification you make to the
     * returned list will be present inside the JAXB object.
     * This is why there is not a <CODE>set</CODE> method for the shift property.
     * 
     * <p>
     * For example, to add a new item, do as follows:
     * <pre>
     *    getShift().add(newItem);
     * </pre>
     * 
     * 
     * <p>
     * Objects of the following type(s) are allowed in the list
     * {@link ShiftType }
     * 
     * 
     */
    public List<ShiftType> getShift() {
        if (shift == null) {
            shift = new ArrayList<ShiftType>();
        }
        return this.shift;
    }

    /**
     * Obtiene el valor de la propiedad rightFragment.
     * 
     * @return
     *     possible object is
     *     {@link RightFragmentType }
     *     
     */
    public RightFragmentType getRightFragment() {
        return rightFragment;
    }

    /**
     * Define el valor de la propiedad rightFragment.
     * 
     * @param value
     *     allowed object is
     *     {@link RightFragmentType }
     *     
     */
    public void setRightFragment(RightFragmentType value) {
        this.rightFragment = value;
    }

    /**
     * Obtiene el valor de la propiedad minFragLength.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getMinFragLength() {
        return minFragLength;
    }

    /**
     * Define el valor de la propiedad minFragLength.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setMinFragLength(String value) {
        this.minFragLength = value;
    }

    /**
     * Obtiene el valor de la propiedad position.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getPosition() {
        return position;
    }

    /**
     * Define el valor de la propiedad position.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setPosition(String value) {
        this.position = value;
    }

    /**
     * Obtiene el valor de la propiedad subSeqMaxOffset.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSeqMaxOffset() {
        return subSeqMaxOffset;
    }

    /**
     * Define el valor de la propiedad subSeqMaxOffset.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSeqMaxOffset(String value) {
        this.subSeqMaxOffset = value;
    }

    /**
     * Obtiene el valor de la propiedad subSeqMinOffset.
     * 
     * @return
     *     possible object is
     *     {@link String }
     *     
     */
    public String getSubSeqMinOffset() {
        return subSeqMinOffset;
    }

    /**
     * Define el valor de la propiedad subSeqMinOffset.
     * 
     * @param value
     *     allowed object is
     *     {@link String }
     *     
     */
    public void setSubSeqMinOffset(String value) {
        this.subSeqMinOffset = value;
    }

}
